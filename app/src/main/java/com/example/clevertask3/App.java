package com.example.clevertask3;

import android.app.Application;

import androidx.room.Room;

import com.example.clevertask3.room.AppDatabase;

public class App extends Application {

    private static final String DATABASE_NAME = "database-name";

    public static App instance;

    private AppDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, DATABASE_NAME)
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public AppDatabase getDatabase() {
        return database;
    }

}
