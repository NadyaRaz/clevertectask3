package com.example.clevertask3;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.clevertask3.room.AppDatabase;
import com.example.clevertask3.room.Contact;
import com.example.clevertask3.room.ContactDao;

import java.util.List;

public class MyContactDialog extends DialogFragment {

    private AppDatabase database = App.getInstance().getDatabase();
    private ContactDao dao = database.contactDao();

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ScrollView layout = (ScrollView) LayoutInflater.from(getContext()).inflate(R.layout.contact_list_layout, null, false);
        Runnable read = () -> {
            List<Contact> saveContact = dao.getAll();
            for (Contact drive : saveContact) {
                getActivity().runOnUiThread(() -> {
                    TextView view = new TextView(getContext());
                    view.setText(drive.phone);
                    int padding = getResources().getDimensionPixelSize(R.dimen.contact_padding);
                    view.setPadding(0, padding, 0, padding);
                    view.setOnClickListener(v -> {
                        ((MainActivity) getActivity()).setContactDetails(drive.fio, drive.phone, drive.email);
                        dismiss();
                    });
                    ((LinearLayout) layout.findViewById(R.id.contacts_dialog_list)).addView(view);
                });
            }
        };
        new Thread(read).start();
        builder.setView(layout);
        return builder.create();
    }
}
