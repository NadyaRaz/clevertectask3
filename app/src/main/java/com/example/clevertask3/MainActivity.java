package com.example.clevertask3;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.clevertask3.room.AppDatabase;
import com.example.clevertask3.room.Contact;
import com.example.clevertask3.room.ContactDao;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {

    private final static int REQUEST_CODE_PERMISSION_READ_CONTACTS = 666;
    private final static int PICK_CONTACT = 777;
    private static final String PHONE_KEY = "PHONE_KEY";
    private Button contactRequestPermissionChooseBtn;
    private Button contactShowBtn;
    private Button contactShowSPBtn;
    private TextView textView;

    private SharedPreferences sPref;
    private AppDatabase database = App.getInstance().getDatabase();
    private ContactDao dao = database.contactDao();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sPref = getPreferences(MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        findViewsById();
        setOnCLickListeners();
    }

    private void findViewsById() {
        contactRequestPermissionChooseBtn = findViewById(R.id.choose_contact);
        contactShowBtn = findViewById(R.id.show_contact);
        contactShowSPBtn = findViewById(R.id.show_contact_from_sp);
        textView = findViewById(R.id.sher_pref_text);
    }

    private void setOnCLickListeners() {
        contactRequestPermissionChooseBtn.setOnClickListener(v -> {
            if (!checkPermission(Manifest.permission.READ_CONTACTS)) {
                requestPermission(Manifest.permission.READ_CONTACTS, REQUEST_CODE_PERMISSION_READ_CONTACTS);
            } else {
                openContacts();
            }
        });

        contactShowBtn.setOnClickListener(v -> {
            (new MyContactDialog()).show(getSupportFragmentManager(), "MY_TAG");
        });
        contactShowSPBtn.setOnClickListener(v -> {
            textView.setText(getPhoneFromSp());
        });
    }

    private void savePhoneToSp(String text) {
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(PHONE_KEY, text);
        ed.apply();
    }

    private String getPhoneFromSp() {
        return sPref.getString(PHONE_KEY, "");
    }

    public void setContactDetails(String fio, String phone, String email) {
        textView.setText(String.format("Имя: %s\nТелефон: %s\nEmail: %s", fio, phone, email));
        savePhoneToSp(phone);
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PERMISSION_GRANTED;
    }

    private void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permission},
                requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_READ_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    openContacts();
                } else {
                    Toast.makeText(this, "Доступ к контактам не был дан", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void openContacts() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        if (reqCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            Uri contactData = data.getData();
            Cursor c = getContentResolver().query(contactData, null, null, null, null);
            if (c.moveToFirst()) {
                String name = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phone = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String email = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                writeRoom(name, phone, email);
            }
            c.close();
        }
    }

    private void writeRoom(String name, String phone, String email) {
        Runnable insert = () -> {
            try {
                Contact contact1 = new Contact();
                contact1.fio = name;
                contact1.phone = phone;
                contact1.email = email;
                dao.insert(contact1);
                runOnUiThread(() -> {
                    Toast.makeText(this, "Сохранено успешно", Toast.LENGTH_LONG).show();
                });
            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(() -> {
                    Toast.makeText(this, "Не удалось записать", Toast.LENGTH_LONG).show();
                });
            }
        };
        new Thread(insert).start();
    }
}
