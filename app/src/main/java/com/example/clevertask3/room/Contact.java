package com.example.clevertask3.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Contact {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo(name = "phone_number")
    public String phone;

    @ColumnInfo(name = "contact_fio")
    public String fio;

    @ColumnInfo(name = "contact_email")
    public String email;
}
